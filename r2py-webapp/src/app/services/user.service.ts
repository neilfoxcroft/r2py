import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/User.Model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  async getUser(user_id: number): Promise<Observable<User>> {
    // let params = new HttpParams();
    // params = params.append('user_id', user_id);
    return this.http.get<User>(`${environment.url}user/${user_id}`);
  }
}
