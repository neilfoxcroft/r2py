import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Team } from '../models/Team.Model';

@Injectable({
  providedIn: 'root',
})
export class TeamService {
  constructor(private http: HttpClient) {}

  async getTeam(team_id: number): Promise<Observable<Team>> {
    // let params = new HttpParams();
    // params = params.append('user_id', user_id);
    return this.http.get<Team>(`${environment.url}team/${team_id}`);
  }
}
