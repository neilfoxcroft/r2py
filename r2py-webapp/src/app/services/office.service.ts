import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Office } from '../models/Office.Model';

@Injectable({
  providedIn: 'root',
})
export class OfficeService {
  constructor(private http: HttpClient) {}

  async getOffice(office_id: number): Promise<Observable<Office>> {
    // let params = new HttpParams();
    // params = params.append('user_id', user_id);
    return this.http.get<Office>(`${environment.url}office/${office_id}`);
  }
}
