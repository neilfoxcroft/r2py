import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from 'src/app/models/User.Model';
import { UserService } from 'src/app/services/user.service';
import { TeamService } from 'src/app/services/team.service';
import { OfficeService } from 'src/app/services/office.service';
import { Team } from 'src/app/models/Team.Model';
import { Office } from 'src/app/models/Office.Model';
import { of } from 'rxjs';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit {
  hidePassword1 = true;
  hidePassword2 = true;
  disableSelect = new FormControl(false);

  user_id = 1;

  user!: User;
  team!: Team;
  office!: Office;

  constructor(
    private userService: UserService,
    private teamService: TeamService,
    private officeService: OfficeService
  ) {}

  async ngOnInit(): Promise<void> {
    (await this.userService.getUser(32)).subscribe((payload) => {
      this.user = payload;
      this.fetchTeam(this.user.team_id);
      console.log(this.user);
      (<HTMLInputElement>document.getElementById('firstNameField')).value =
        this.user.first_name;
      (<HTMLInputElement>document.getElementById('lastNameField')).value =
        this.user.last_name;
      (<HTMLInputElement>document.getElementById('emailField')).value =
        this.user.email_address;
      (<HTMLInputElement>document.getElementById('passwordField')).value =
        this.user.password;
      (<HTMLInputElement>document.getElementById('passwordCField')).value =
        this.user.password;
    });

    // (await this.teamService.getTeam(this.user.team_id)).subscribe((paylaod) => {
    //   this.team = paylaod;
    //   console.log(this.team);
    // });

    // (await this.officeService.getOffice(this.team.office_id)).subscribe(
    //   (paylaod) => {
    //     this.office = paylaod;
    //     console.log(this.office);
    //   }
    // );
  }

  async fetchTeam(team_id: number) {
    (await this.teamService.getTeam(team_id)).subscribe((payload) => {
      this.team = payload;
      this.fetchOffice(this.team.office_id);
      console.log(this.team);
      (<HTMLInputElement>document.getElementById('teamField')).value =
        this.team.team_name.toString();
    });
  }

  async fetchOffice(office_id: number) {
    (await this.officeService.getOffice(office_id)).subscribe((paylaod) => {
      this.office = paylaod;
      console.log(this.office);
      (<HTMLInputElement>document.getElementById('officeField')).value =
        this.office.office_name.toString();
    });
  }
}
