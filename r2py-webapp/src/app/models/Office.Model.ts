export interface Office {
  office_id: number;
  office_name: string;
  office_address: string;
}
