export interface Team {
  team_id: number;
  office_id: number;
  team_name: string;
}
