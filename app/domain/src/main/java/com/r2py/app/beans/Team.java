package com.r2py.app.beans;

import lombok.Data;

import java.util.List;

@Data
public class Team {
    private Integer team_id;
    private Integer office_id;
    private String team_name;
    private List<User> users;
}