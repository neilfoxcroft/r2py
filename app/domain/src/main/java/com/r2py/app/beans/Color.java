package com.r2py.app.beans;

import lombok.Data;

@Data
public class Color {
    private Integer color_id;
    private String color;
}
