package com.r2py.app.beans;

import lombok.Data;

import java.util.List;

@Data
public class Office {
    private Integer office_id;
    private String office_name;
    private String office_address;
    private List<Desk> desks;
}
