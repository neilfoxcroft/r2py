package com.r2py.app.beans;

import lombok.Data;

@Data
public class User {
    private Integer user_id;
    private String first_name;
    private String last_name;
    private String email_address;
    private String password;
    private Integer team_id;
}
