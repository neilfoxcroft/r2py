package com.r2py.app.beans;

import lombok.Data;

@Data
public class Booking {
    private Integer booking_id;
    private Integer desk_id;
    private Integer user_id;
    private Integer day_id;
    private String reference_number;
}