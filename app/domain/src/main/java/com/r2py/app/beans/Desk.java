package com.r2py.app.beans;

import lombok.Data;

@Data
public class Desk {
    private Integer desk_id;
    private String desk_number;
    private Integer x_cord;
    private Integer y_cord;
    private Integer group_number;
    private Boolean booked;
    private Integer office_id;
    private Integer color_id;
}