package com.r2py.app.beans;

import lombok.Data;

import java.util.Date;

@Data
public class Day {
    private Integer day_id;
    private Date date;
}