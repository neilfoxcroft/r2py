package com.r2py.app.controllers;

import com.r2py.app.beans.Booking;
import com.r2py.app.handlers.BookingHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/booking/")
public class BookingController {

    BookingHandler bookingHandler;

    @Autowired
    public BookingController(BookingHandler bookingHandler){
        this.bookingHandler = bookingHandler;
    }

    @GetMapping("all")
    public ResponseEntity<List<Booking>> getAllBookings(){
        List<Booking> allBookingsList = bookingHandler.getAllBookings();
        return ResponseEntity.ok(allBookingsList);
    }

    @GetMapping("{bookingID}")
    public ResponseEntity<Booking> getBookingById(@PathVariable Integer bookingID){
        Booking requestedBooking = bookingHandler.getBooking(bookingID);
        return ResponseEntity.ok(requestedBooking);
    }

    @PostMapping("add")
    public ResponseEntity<Booking> addBooking(@RequestBody Booking booking){
        return new ResponseEntity<>(bookingHandler.createBooking(booking), HttpStatus.OK);
    }

    @DeleteMapping("delete/{booking_id}")
    public ResponseEntity<Booking> deleteSpecificBooking(@PathVariable Integer booking_id){
        Booking bookingToBeDeleted = bookingHandler.getBooking(booking_id);
        return new ResponseEntity<>(bookingHandler.delete(bookingToBeDeleted), HttpStatus.OK);
    }

    @PatchMapping("update")
    public ResponseEntity<Booking> updateBooking(@RequestBody Booking booking){
        return new ResponseEntity<>(bookingHandler.updateBooking(booking), HttpStatus.OK);
    }
}
