package com.r2py.app.security;


import com.r2py.app.beans.User;
import com.r2py.app.handlers.UserHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class r2UserDetailsService implements UserDetailsService {
    private final UserHandler userService;

    @Autowired
    public r2UserDetailsService(UserHandler userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user;
        try {
            user = userService.findByEmail(email);
        } catch (UsernameNotFoundException exception) {
            throw new UsernameNotFoundException(email);
        }
        return new UserPrincipal(user);
    }
}
