package com.r2py.app.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class UserDTO {
    private String email_address;
    private String password;
}
