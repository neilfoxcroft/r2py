package com.r2py.app.controllers;

import com.r2py.app.beans.Team;
import com.r2py.app.handlers.TeamHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/team/")
public class TeamController {

    TeamHandler teamHandler;

    @Autowired
    public TeamController(TeamHandler teamHandler){
        this.teamHandler = teamHandler;
    }

    @GetMapping("all")
    public ResponseEntity<List<Team>> getAllTeams(){
        List<Team> allTeamsList = teamHandler.getAllTeams();
        return ResponseEntity.ok(allTeamsList);
    }

    @GetMapping("{teamID}")
    public ResponseEntity<Team> getTeamById(@PathVariable Integer teamID){
        Team requestedTeam = teamHandler.getTeam(teamID);
        return ResponseEntity.ok(requestedTeam);
    }

    @PostMapping("add")
    public ResponseEntity<Team> addTeam(@RequestBody Team team){
        return new ResponseEntity<>(teamHandler.createTeam(team), HttpStatus.OK);
    }

    @DeleteMapping("delete/{team_id}")
    public ResponseEntity<Team> deleteSpecificTeam(@PathVariable Integer team_id){
        Team teamToBeDeleted = teamHandler.getTeam(team_id);
        return new ResponseEntity<>(teamHandler.delete(teamToBeDeleted), HttpStatus.OK);
    }

    @PatchMapping("update")
    public ResponseEntity<Team> updateTeam(@RequestBody Team team){
        return new ResponseEntity<>(teamHandler.updateTeam(team), HttpStatus.OK);
    }
}
