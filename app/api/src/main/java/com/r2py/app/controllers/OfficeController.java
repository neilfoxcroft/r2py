package com.r2py.app.controllers;

import com.r2py.app.beans.Office;
import com.r2py.app.handlers.OfficeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/office/")
public class OfficeController {

    OfficeHandler officeHandler;

    @Autowired
    public OfficeController(OfficeHandler officeHandler){
        this.officeHandler = officeHandler;
    }

    @GetMapping("all")
    public ResponseEntity<List<Office>> getAllOffices(){
        List<Office> allOfficesList = officeHandler.getAllOffices();
        return ResponseEntity.ok(allOfficesList);
    }

    @GetMapping("{officeID}")
    public ResponseEntity<Office> getOfficeById(@PathVariable Integer officeID){
        Office requestedOffice = officeHandler.getOffice(officeID);
        return ResponseEntity.ok(requestedOffice);
    }

    @PostMapping("add")
    public ResponseEntity<Office> addOffice(@RequestBody Office office){
        return new ResponseEntity<>(officeHandler.createOffice(office), HttpStatus.OK);
    }

    @DeleteMapping("delete/{office_id}")
    public ResponseEntity<Office> deleteSpecificOffice(@PathVariable Integer office_id){
        Office officeToBeDeleted = officeHandler.getOffice(office_id);
        return new ResponseEntity<>(officeHandler.delete(officeToBeDeleted), HttpStatus.OK);
    }

    @PatchMapping("update")
    public ResponseEntity<Office> updateOffice(@RequestBody Office office){
        return new ResponseEntity<>(officeHandler.updateOffice(office), HttpStatus.OK);
    }
}
