package com.r2py.app.controllers;

import com.r2py.app.beans.Desk;
import com.r2py.app.handlers.DeskHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/desk/")
public class DeskController {

    DeskHandler deskHandler;

    @Autowired
    public DeskController(DeskHandler deskHandler){
        this.deskHandler = deskHandler;
    }

    //TODO: Fix GET ALL - Desk
    @GetMapping("all")
    public ResponseEntity<List<Desk>> getAllDesks(){
        List<Desk> allDesksList = deskHandler.getAllDesks();
        return ResponseEntity.ok(allDesksList);
    }

    @GetMapping("{deskID}")
    public ResponseEntity<Desk> getDeskById(@PathVariable Integer deskID){
        Desk requestedDesk = deskHandler.getDesk(deskID);
        return ResponseEntity.ok(requestedDesk);
    }

    @PostMapping("add")
    public ResponseEntity<Desk> addDesk(@RequestBody Desk desk){
        return new ResponseEntity<>(deskHandler.createDesk(desk), HttpStatus.OK);
    }

    @DeleteMapping("delete/{desk_id}")
    public ResponseEntity<Desk> deleteSpecificDesk(@PathVariable Integer desk_id){
        Desk deskToBeDeleted = deskHandler.getDesk(desk_id);
        return new ResponseEntity<>(deskHandler.delete(deskToBeDeleted), HttpStatus.OK);
    }

    @PatchMapping("update")
    public ResponseEntity<Desk> updateDesk(@RequestBody Desk desk){
        return new ResponseEntity<>(deskHandler.updateDesk(desk), HttpStatus.OK);
    }
}
