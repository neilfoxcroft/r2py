package com.r2py.app.controllers;

import com.r2py.app.beans.Day;
import com.r2py.app.handlers.DayHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/day/")
public class DayController {

    DayHandler dayHandler;

    @Autowired
    public DayController(DayHandler dayHandler){
        this.dayHandler = dayHandler;
    }

    @GetMapping("all")
    public ResponseEntity<List<Day>> getAllDays(){
        List<Day> allDaysList = dayHandler.getAllDays();
        return ResponseEntity.ok(allDaysList);
    }

    @GetMapping("{dayID}")
    public ResponseEntity<Day> getDayById(@PathVariable Integer dayID){
        Day requestedDay = dayHandler.getDay(dayID);
        return ResponseEntity.ok(requestedDay);
    }
}
