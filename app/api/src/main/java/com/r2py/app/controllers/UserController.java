package com.r2py.app.controllers;

import com.r2py.app.beans.User;
import com.r2py.app.handlers.UserHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user/")
public class UserController {

    UserHandler userHandler;

    @Autowired
    public UserController(UserHandler userHandler){
        this.userHandler = userHandler;
    }

    @GetMapping("all")
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> allUsersList = userHandler.getAllUsers();
        return ResponseEntity.ok(allUsersList);
    }

    @GetMapping("{userID}")
    public ResponseEntity<User> getUserById(@PathVariable Integer userID){
        User requestedUser = userHandler.getUser(userID);
        return ResponseEntity.ok(requestedUser);
    }

    @PostMapping("add")
    public ResponseEntity<User> addUser(@RequestBody User user){
        return new ResponseEntity<>(userHandler.createUser(user), HttpStatus.OK);
    }

    @DeleteMapping("delete/{user_id}")
    public ResponseEntity<User> deleteSpecificUser(@PathVariable Integer user_id){
        User userToBeDeleted = userHandler.getUser(user_id);
        return new ResponseEntity<>(userHandler.delete(userToBeDeleted), HttpStatus.OK);
    }

    @PatchMapping("update")
    public ResponseEntity<User> updateUser(@RequestBody User user){
        return new ResponseEntity<>(userHandler.updateUser(user), HttpStatus.OK);
    }
}
