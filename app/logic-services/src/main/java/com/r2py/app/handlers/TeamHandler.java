package com.r2py.app.handlers;

import com.r2py.app.beans.Team;
import com.r2py.app.repos.TeamRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan("com.r2py.app.mappers")
public class TeamHandler {

    private final TeamRepo teamRepo;

    @Autowired
    public TeamHandler(TeamRepo teamRepo) {
        this.teamRepo = teamRepo;
    }

    public Team createTeam(Team team) {
        return teamRepo.create(team);
    }

    public Team getTeam(int team_id) {
        return teamRepo.findById(team_id);
    }

    public List<Team> getAllTeams() {
        return teamRepo.getAll();
    }

    public Team delete(Team team) {
        return teamRepo.delete(team);
    }

    public Team updateTeam(Team team) {
        return teamRepo.update(team);
    }

}
