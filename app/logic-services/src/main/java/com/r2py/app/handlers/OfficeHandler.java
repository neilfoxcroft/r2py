package com.r2py.app.handlers;

import com.r2py.app.beans.Office;
import com.r2py.app.repos.OfficeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan("com.r2py.app.mappers")
public class OfficeHandler {

    private final OfficeRepo officeRepo;

    @Autowired
    public OfficeHandler(OfficeRepo officeRepo) {
        this.officeRepo = officeRepo;
    }

    public Office createOffice(Office office) {
        return officeRepo.create(office);
    }

    public Office getOffice(int office_id) {
        return officeRepo.findById(office_id);
    }

    public List<Office> getAllOffices() {
        return officeRepo.getAll();
    }

    public Office delete(Office office) {
        return officeRepo.delete(office);
    }

    public Office updateOffice(Office office) {
        return officeRepo.update(office);
    }

}
