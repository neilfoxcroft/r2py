package com.r2py.app.handlers;

import com.r2py.app.beans.Desk;
import com.r2py.app.repos.DeskRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan("com.r2py.app.mappers")
public class DeskHandler {

    private final DeskRepo deskRepo;

    @Autowired
    public DeskHandler(DeskRepo deskRepo) {
        this.deskRepo = deskRepo;
    }

    public Desk createDesk(Desk desk) {
        return deskRepo.create(desk);
    }

    public Desk getDesk(int desk_id) {
        return deskRepo.findById(desk_id);
    }

    public List<Desk> getAllDesks() {
        return deskRepo.getAll();
    }

    public Desk delete(Desk desk) {
        return deskRepo.delete(desk);
    }

    public Desk updateDesk(Desk desk) {
        return deskRepo.update(desk);
    }

}
