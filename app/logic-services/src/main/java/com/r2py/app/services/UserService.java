package com.r2py.app.services;

import com.r2py.app.beans.User;
import com.r2py.app.mappers.UserMapper;
import com.r2py.app.repos.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserService {

    private UserRepo userRepo;
    private UserMapper userMapper;

    private Logger logger = LoggerFactory.getLogger(UserRepo.class.getName());

    @Autowired
    public UserService(UserRepo userRepo, UserMapper userMapper) {
        this.userRepo = userRepo;
        this.userMapper = userMapper;
    }

    public User findByEmail(String email) {
        return userRepo.findByEmail(email);
    }
}
