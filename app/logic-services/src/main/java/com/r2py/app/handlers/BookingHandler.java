package com.r2py.app.handlers;

import com.r2py.app.beans.Booking;
import com.r2py.app.repos.BookingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan("com.r2py.app.mappers")
public class BookingHandler {

    private final BookingRepo bookingRepo;

    @Autowired
    public BookingHandler(BookingRepo bookingRepo) {
        this.bookingRepo = bookingRepo;
    }

    public Booking createBooking(Booking booking) {
        return bookingRepo.create(booking);
    }

    public Booking getBooking(int booking_id) {
        return bookingRepo.findById(booking_id);
    }

    public List<Booking> getAllBookings() {
        return bookingRepo.getAll();
    }

    public Booking delete(Booking booking) {
        return bookingRepo.delete(booking);
    }

    public Booking updateBooking(Booking booking) {
        return bookingRepo.update(booking);
    }

}
