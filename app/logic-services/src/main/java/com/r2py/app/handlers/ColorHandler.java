package com.r2py.app.handlers;

import com.r2py.app.beans.Color;
import com.r2py.app.repos.ColorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan("com.r2py.app.mappers")
public class ColorHandler {

    private final ColorRepo colorRepo;

    @Autowired
    public ColorHandler(ColorRepo colorRepo) {
        this.colorRepo = colorRepo;
    }

    public Color createColor(Color color) {
        return colorRepo.create(color);
    }

    public Color getColor(int color_id) {
        return colorRepo.findById(color_id);
    }

    public List<Color> getAllColors() {
        return colorRepo.getAll();
    }

    public Color delete(Color color) {
        return colorRepo.delete(color);
    }

    public Color updateColor(Color color) {
        return colorRepo.update(color);
    }

}
