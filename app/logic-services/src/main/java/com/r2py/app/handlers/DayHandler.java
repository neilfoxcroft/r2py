package com.r2py.app.handlers;

import com.r2py.app.beans.Day;
import com.r2py.app.repos.DayRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan("com.r2py.app.mappers")
public class DayHandler {

    private final DayRepo dayRepo;

    @Autowired
    public DayHandler(DayRepo dayRepo) {
        this.dayRepo = dayRepo;
    }

    public Day getDay(int id) {
        return dayRepo.findById(id);
    }

    public List<Day> getAllDays() {
        return dayRepo.getAll();
    }
}
