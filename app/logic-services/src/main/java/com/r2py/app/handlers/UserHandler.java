package com.r2py.app.handlers;

import com.r2py.app.beans.User;
import com.r2py.app.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ComponentScan("com.r2py.app.mappers")
public class UserHandler {

    private final UserRepo userRepo;

    @Autowired
    public UserHandler(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public User createUser(User user) {
        return userRepo.create(user);
    }

    public User getUser(int user_id) {
        return userRepo.findById(user_id);
    }

    public List<User> getAllUsers() {
        return userRepo.getAll();
    }

    public User delete(User user) {
        return userRepo.delete(user);
    }

    public User updateUser(User user) {
        return userRepo.update(user);
    }

    public User findByEmail(String email) {
        return userRepo.findByEmail(email);
    }
}
