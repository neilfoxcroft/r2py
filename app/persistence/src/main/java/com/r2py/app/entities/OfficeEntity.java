package com.r2py.app.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "offices")
@Getter
@Setter
public class OfficeEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer office_id;

    @Column(name = "office_name", columnDefinition = "nvarchar")
    private String office_name;

    @Column(name = "office_address", columnDefinition = "nvarchar")
    private String office_address;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @JoinColumn(name = "office_id")
//    private List<TeamEntity> teamEntities;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @JoinColumn(name = "office_id")
//    private List<DeskEntity> deskEntities;
}
