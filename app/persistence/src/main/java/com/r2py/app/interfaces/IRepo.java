package com.r2py.app.interfaces;

import java.util.List;

public interface IRepo<TEntity, TKey> {
    TEntity create(TEntity entity);
    TEntity update(TEntity entity);
    TEntity delete(TEntity entity);
    TEntity findById(TKey id);
    List<TEntity> getAll();
}
