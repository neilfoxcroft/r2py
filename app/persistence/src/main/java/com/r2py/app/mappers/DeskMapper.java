package com.r2py.app.mappers;

import com.r2py.app.beans.Desk;
import com.r2py.app.entities.DeskEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DeskMapper {

    public Desk toDeskObject(DeskEntity deskEntity) {
        if (deskEntity == null) {
            return null;
        }
        Desk deskObject = new Desk();
        if (deskEntity.getDesk_id() != null) {
            deskObject.setDesk_id(deskEntity.getDesk_id());
        }
        deskObject.setDesk_number(deskEntity.getDesk_number());
        deskObject.setX_cord(deskEntity.getX_cord());
        deskObject.setY_cord(deskEntity.getY_cord());
        deskObject.setGroup_number(deskEntity.getGroup_number());
        deskObject.setBooked(deskEntity.getBooked());
        deskObject.setOffice_id(deskEntity.getOffice_id());
        deskObject.setColor_id(deskEntity.getColor_id());
        return deskObject;
    }

    public DeskEntity toDeskEntity(Desk deskObject) {
        if (deskObject == null) {
            return null;
        }
        DeskEntity deskEntity = new DeskEntity();
        if (deskObject.getDesk_id() != null) {
            deskEntity.setDesk_id(deskObject.getDesk_id());
        }
        deskEntity.setDesk_number(deskObject.getDesk_number());
        deskEntity.setX_cord(deskObject.getX_cord());
        deskEntity.setY_cord(deskObject.getY_cord());
        deskEntity.setGroup_number(deskObject.getGroup_number());
        deskEntity.setBooked(deskObject.getBooked());
        deskEntity.setOffice_id(deskObject.getOffice_id());
        deskEntity.setColor_id(deskObject.getColor_id());
        return deskEntity;
    }

    public List<Desk> toDeskObjectList(List<DeskEntity> deskEntityList) {
        if (deskEntityList == null) {
            return null;
        }
        List<Desk> deskObjectList = new ArrayList<>();
        for (DeskEntity deskEntity : deskEntityList) {
            deskObjectList.add(toDeskObject(deskEntity));
        }
        return deskObjectList;
    }

    public List<DeskEntity> toDeskEntityList(List<Desk> deskObjectList) {
        if (deskObjectList == null) {
            return null;
        }
        List<DeskEntity> deskEntityList = new ArrayList<>();
        for (Desk deskObject : deskObjectList) {
            deskEntityList.add(toDeskEntity(deskObject));
        }
        return deskEntityList;
    }

    public void updateDeskEntity(DeskEntity deskEntity, Desk desk) {
        if (desk.getDesk_id() != null) {
            deskEntity.setDesk_id(desk.getDesk_id());
        }
        deskEntity.setDesk_number(desk.getDesk_number());
        deskEntity.setX_cord(desk.getX_cord());
        deskEntity.setY_cord(desk.getY_cord());
        deskEntity.setGroup_number(desk.getGroup_number());
        deskEntity.setBooked(desk.getBooked());
        deskEntity.setOffice_id(desk.getOffice_id());
        deskEntity.setColor_id(desk.getColor_id());
    }
}