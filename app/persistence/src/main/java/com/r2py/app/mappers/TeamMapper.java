package com.r2py.app.mappers;

import com.r2py.app.beans.Team;
import com.r2py.app.entities.TeamEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TeamMapper {

    private UserMapper userMapper;

    @Autowired
    public TeamMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }


    public Team toTeamObject(TeamEntity teamEntity) {
        if (teamEntity == null) {
            return null;
        }
        Team teamObject = new Team();
        if (teamEntity.getTeam_id() != null) {
            teamObject.setTeam_id(teamEntity.getTeam_id());
        }
        teamObject.setOffice_id(teamEntity.getOffice_id());
        teamObject.setTeam_name(teamEntity.getTeam_name());
        teamObject.setUsers(userMapper.toUserObjectList(teamEntity.getUserEntities()));
        return teamObject;
    }

    public TeamEntity toTeamEntity(Team teamObject) {
        if (teamObject == null) {
            return null;
        }
        TeamEntity teamEntity = new TeamEntity();
        if (teamObject.getTeam_id() != null) {
            teamEntity.setTeam_id(teamObject.getTeam_id());
        }
        teamEntity.setOffice_id(teamObject.getOffice_id());
        teamEntity.setTeam_name(teamObject.getTeam_name());
        return teamEntity;
    }

    public List<Team> toTeamObjectList(List<TeamEntity> teamEntityList) {
        if (teamEntityList == null) {
            return null;
        }
        List<Team> teamObjectList = new ArrayList<>();
        for (TeamEntity teamEntity : teamEntityList) {
            teamObjectList.add(toTeamObject(teamEntity));
        }
        return teamObjectList;
    }

    public List<TeamEntity> toTeamEntityList(List<Team> teamObjectList) {
        if (teamObjectList == null) {
            return null;
        }
        List<TeamEntity> teamEntityList = new ArrayList<>();
        for (Team teamObject : teamObjectList) {
            teamEntityList.add(toTeamEntity(teamObject));
        }
        return teamEntityList;
    }

    public void updateTeamEntity(TeamEntity teamEntity, Team team) {
        if (team.getTeam_id() != null) {
            teamEntity.setTeam_id(team.getTeam_id());
        }
        teamEntity.setOffice_id(team.getOffice_id());
        teamEntity.setTeam_name(team.getTeam_name());
    }
}