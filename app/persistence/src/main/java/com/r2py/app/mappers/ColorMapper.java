package com.r2py.app.mappers;

import com.r2py.app.beans.Color;
import com.r2py.app.entities.ColorEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ColorMapper {

    public Color toColorObject(ColorEntity colorEntity) {
        if (colorEntity == null) {
            return null;
        }
        Color colorObject = new Color();
        if (colorEntity.getColor_id() != null) {
            colorObject.setColor_id(colorEntity.getColor_id());
        }
        colorObject.setColor(colorEntity.getColor());
        return colorObject;
    }

    public ColorEntity toColorEntity(Color colorObject) {
        if (colorObject == null) {
            return null;
        }
        ColorEntity colorEntity = new ColorEntity();
        if (colorObject.getColor_id() != null) {
            colorEntity.setColor_id(colorObject.getColor_id());
        }
        colorEntity.setColor(colorObject.getColor());
        return colorEntity;
    }

    public List<Color> toColorObjectList(List<ColorEntity> colorEntityList) {
        if (colorEntityList == null) {
            return null;
        }
        List<Color> colorObjectList = new ArrayList<>();
        for (ColorEntity colorEntity : colorEntityList) {
            colorObjectList.add(toColorObject(colorEntity));
        }
        return colorObjectList;
    }

    public List<ColorEntity> toColorEntityList(List<Color> colorObjectList) {
        if (colorObjectList == null) {
            return null;
        }
        List<ColorEntity> colorEntityList = new ArrayList<>();
        for (Color colorObject : colorObjectList) {
            colorEntityList.add(toColorEntity(colorObject));
        }
        return colorEntityList;
    }

    public void updateColorEntity(ColorEntity colorEntity, Color color) {
        if (color.getColor_id() != null) {
            colorEntity.setColor_id(color.getColor_id());
        }
        colorEntity.setColor(color.getColor());
    }
}