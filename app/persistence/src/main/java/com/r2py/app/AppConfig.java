/*
package com.r2py.app.entities.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class AppConfig {

    @Value("${hibernate.show_sql}")
    private String showSql;

    @Value("${hibernate.format_sql}")
    private String formatSql;

    @Value("${hibernate.generate_statistics}")
    private String generateStatistics;

    @Value("${hibernate.connection.driver_class}")
    private String driverClass;

    @Value("${hibernate.connection.url}")
    private String url;

    @Value("${hibernate.connection.username}")
    private String username;

    @Value("${hibernate.connection.password}")
    private String password;

    @Value("${hibernate.dialect}")
    private String dialect;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hd3ddl;

    @Bean
    public EntityManager entityManager(){
        Map<String,String> props = new HashMap<>();
        props.put("hibernate.show_sql",showSql);
        props.put("hibernate.format_sql",formatSql);
        props.put("hibernate.generate_statistics",generateStatistics);
        props.put("hibernate.connection.driver_class",driverClass);
        props.put("hibernate.connection.url",url);
        props.put("hibernate.connection.username",username);
        props.put("hibernate.connection.password",password);
        props.put("hibernate.dialect",dialect);
        props.put("hibernate.hbm2ddl.auto",hd3ddl);
        return Persistence.createEntityManagerFactory("r2pyPersistenceUnit",props).createEntityManager();
    }
}
*/

package com.r2py.app;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class AppConfig {

    @Bean
    public EntityManager entityManager(){
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("hibernate.show_sql","true");
        properties.put("hibernate.format_sql","true");
        properties.put("hibernate.generate_statistics","true");
        properties.put("hibernate.connection.driver_class","com.microsoft.sqlserver.jdbc.SQLServerDriver");
        properties.put("hibernate.connection.url","jdbc:sqlserver://r2pyserver.database.windows.net:1433;database=r2py;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;");
        properties.put("hibernate.connection.username","r2admin");
        properties.put("hibernate.connection.password","Wagwoord@123");
        properties.put("hibernate.dialect","org.hibernate.dialect.SQLServerDialect");
        properties.put("hibernate.hbm2ddl.auto","validate");
        System.out.println(properties.get("hibernate.connection.driver_class"));
        return Persistence.createEntityManagerFactory("r2pyPersistenceUnit",properties).createEntityManager();
    }
}
