package com.r2py.app.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "days")
@Getter
@Setter
public class DayEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer day_id;

    @Column(name = "date", columnDefinition = "date")
    private Date date;
}
