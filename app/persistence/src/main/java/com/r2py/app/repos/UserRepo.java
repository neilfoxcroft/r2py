package com.r2py.app.repos;

import com.r2py.app.beans.User;
import com.r2py.app.entities.UserEntity;
import com.r2py.app.interfaces.IRepo;
import com.r2py.app.mappers.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class UserRepo implements IRepo<User, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IRepo.class.getName());

    @PersistenceContext
    EntityManager entityManager;
    UserMapper userMapper;

    @Autowired
    public UserRepo(UserMapper userMapper, EntityManager entityManager) {
        this.userMapper = userMapper;
        this.entityManager = entityManager;
    }

    @Transactional
    public User create(User user) {
        UserEntity userEntity = userMapper.toUserEntity(user);
        entityManager.persist(userEntity);
        LOGGER.debug("Created user with ID: " + userEntity.getUser_id());
        return userMapper.toUserObject(userEntity);
    }

    @Transactional
    public User delete(User user) {
        UserEntity userEntity = userMapper.toUserEntity(user);
        entityManager.remove(entityManager.contains(userEntity) ? userEntity : entityManager.merge(userEntity));
        return userMapper.toUserObject(userEntity);
    }

    @Transactional
    public User update(User user) {
        UserEntity userEntity;
        if (user.getUser_id() == null){
            userEntity = userMapper.toUserEntity(user);
        } else {
            userEntity = entityManager.find(UserEntity.class, user.getUser_id());
            userMapper.updateUserEntity(userEntity, user);
        }
        entityManager.persist(userEntity);
        return userMapper.toUserObject(userEntity);
    }

    @Override
    public User findById(Integer id) {
        UserEntity userEntity = entityManager.find(UserEntity.class, id);
        if (userEntity == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return userMapper.toUserObject(userEntity);
    }

    @Transactional
    public User findByEmail(String email) {
        Query query = entityManager.createQuery(
                "SELECT u FROM UserEntity u WHERE u.email_address = ?1").setParameter(1,email);

        UserEntity userEntity = ((UserEntity) query.getSingleResult());
        return userMapper.toUserObject(userEntity);
    }

    @Override
    public List<User> getAll() {
        List<UserEntity> userEntities = entityManager.createQuery("SELECT user FROM UserEntity user").getResultList();
        if (userEntities == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return userMapper.toUserObjectList(userEntities);
    }
}
