package com.r2py.app.repos;

import com.r2py.app.beans.Office;
import com.r2py.app.entities.OfficeEntity;
import com.r2py.app.interfaces.IRepo;
import com.r2py.app.mappers.OfficeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class OfficeRepo implements IRepo<Office, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IRepo.class.getName());

    @PersistenceContext
    EntityManager entityManager;
    OfficeMapper officeMapper;

    @Autowired
    public OfficeRepo(OfficeMapper officeMapper, EntityManager entityManager) {
        this.officeMapper = officeMapper;
        this.entityManager = entityManager;
    }

    @Transactional
    public Office create(Office office) {
        OfficeEntity officeEntity = officeMapper.toOfficeEntity(office);
        entityManager.persist(officeEntity);
        LOGGER.debug("Created office with ID: " + officeEntity.getOffice_id());
        return officeMapper.toOfficeObject(officeEntity);
    }

    @Transactional
    public Office delete(Office office) {
        OfficeEntity officeEntity = officeMapper.toOfficeEntity(office);
        entityManager.remove(entityManager.contains(officeEntity) ? officeEntity : entityManager.merge(officeEntity));
        return officeMapper.toOfficeObject(officeEntity);
    }

    @Transactional
    public Office update(Office office) {
        OfficeEntity officeEntity;
        if (office.getOffice_id() == null){
            officeEntity = officeMapper.toOfficeEntity(office);
        } else {
            officeEntity = entityManager.find(OfficeEntity.class, office.getOffice_id());
            officeMapper.updateOfficeEntity(officeEntity, office);
        }
        entityManager.persist(officeEntity);
        return officeMapper.toOfficeObject(officeEntity);
    }

    @Override
    public Office findById(Integer id) {
        OfficeEntity officeEntity = entityManager.find(OfficeEntity.class, id);
        if (officeEntity == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return officeMapper.toOfficeObject(officeEntity);
    }

    @Override
    public List<Office> getAll() {
        return entityManager.createQuery("select office from OfficeEntity office").getResultList();
    }
}
