package com.r2py.app.mappers;

import com.r2py.app.beans.Office;
import com.r2py.app.entities.OfficeEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OfficeMapper {

    public Office toOfficeObject(OfficeEntity officeEntity) {
        if (officeEntity == null) {
            return null;
        }
        Office officeObject = new Office();
        if (officeEntity.getOffice_id() != null) {
            officeObject.setOffice_id(officeEntity.getOffice_id());
        }
        officeObject.setOffice_name(officeEntity.getOffice_name());
        officeObject.setOffice_address(officeEntity.getOffice_address());
        return officeObject;
    }

    public OfficeEntity toOfficeEntity(Office officeObject) {
        if (officeObject == null) {
            return null;
        }
        OfficeEntity officeEntity = new OfficeEntity();
        if (officeObject.getOffice_id() != null) {
            officeEntity.setOffice_id(officeObject.getOffice_id());
        }
        officeEntity.setOffice_name(officeObject.getOffice_name());
        officeEntity.setOffice_address(officeObject.getOffice_address());
        return officeEntity;
    }

    public List<Office> toOfficeObjectList(List<OfficeEntity> officeEntityList) {
        if (officeEntityList == null) {
            return null;
        }
        List<Office> officeObjectList = new ArrayList<>();
        for (OfficeEntity officeEntity : officeEntityList) {
            officeObjectList.add(toOfficeObject(officeEntity));
        }
        return officeObjectList;
    }

    public List<OfficeEntity> toOfficeEntityList(List<Office> officeObjectList) {
        if (officeObjectList == null) {
            return null;
        }
        List<OfficeEntity> officeEntityList = new ArrayList<>();
        for (Office officeObject : officeObjectList) {
            officeEntityList.add(toOfficeEntity(officeObject));
        }
        return officeEntityList;
    }

    public void updateOfficeEntity(OfficeEntity officeEntity, Office office) {
        if (office.getOffice_id() != null) {
            officeEntity.setOffice_id(office.getOffice_id());
        }
        officeEntity.setOffice_name(office.getOffice_name());
        officeEntity.setOffice_address(office.getOffice_address());
    }
}