package com.r2py.app.repos;

import com.r2py.app.beans.Desk;
import com.r2py.app.entities.DeskEntity;
import com.r2py.app.interfaces.IRepo;
import com.r2py.app.mappers.DeskMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class DeskRepo implements IRepo<Desk, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IRepo.class.getName());

    @PersistenceContext
    EntityManager entityManager;
    DeskMapper deskMapper;

    @Autowired
    public DeskRepo(DeskMapper deskMapper, EntityManager entityManager) {
        this.deskMapper = deskMapper;
        this.entityManager = entityManager;
    }

    @Transactional
    public Desk create(Desk desk) {
        DeskEntity deskEntity = deskMapper.toDeskEntity(desk);
        entityManager.persist(deskEntity);
        LOGGER.debug("Created desk with ID: " + deskEntity.getDesk_id());
        return deskMapper.toDeskObject(deskEntity);
    }

    @Transactional
    public Desk delete(Desk desk) {
        DeskEntity deskEntity = deskMapper.toDeskEntity(desk);
        entityManager.remove(entityManager.contains(deskEntity) ? deskEntity : entityManager.merge(deskEntity));
        return deskMapper.toDeskObject(deskEntity);
    }

    @Transactional
    public Desk update(Desk desk) {
        DeskEntity deskEntity;
        if (desk.getDesk_id() == null){
            deskEntity = deskMapper.toDeskEntity(desk);
        } else {
            deskEntity = entityManager.find(DeskEntity.class, desk.getDesk_id());
            deskMapper.updateDeskEntity(deskEntity, desk);
        }
        entityManager.persist(deskEntity);
        return deskMapper.toDeskObject(deskEntity);
    }

    @Override
    public Desk findById(Integer id) {
        DeskEntity deskEntity = entityManager.find(DeskEntity.class, id);
        if (deskEntity == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return deskMapper.toDeskObject(deskEntity);
    }

    @Override
    public List<Desk> getAll() {
        return entityManager.createQuery("select desk from DeskEntity desk").getResultList();
    }
}
