package com.r2py.app.mappers;

import com.r2py.app.beans.Booking;
import com.r2py.app.entities.BookingEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookingMapper {

    public Booking toBookingObject(BookingEntity bookingEntity) {
        if (bookingEntity == null) {
            return null;
        }
        Booking bookingObject = new Booking();
        if (bookingEntity.getBooking_id() != null) {
            bookingObject.setBooking_id(bookingEntity.getBooking_id());
        }
        bookingObject.setDesk_id(bookingEntity.getDesk_id());
        bookingObject.setUser_id(bookingEntity.getUser_id());
        bookingObject.setDay_id(bookingEntity.getDay_id());
        bookingObject.setReference_number(bookingEntity.getReference_number());
        return bookingObject;
    }

    public BookingEntity toBookingEntity(Booking bookingObject) {
        if (bookingObject == null) {
            return null;
        }
        BookingEntity bookingEntity = new BookingEntity();
        if (bookingObject.getBooking_id() != null) {
            bookingEntity.setBooking_id(bookingObject.getBooking_id());
        }
        bookingEntity.setDesk_id(bookingObject.getDesk_id());
        bookingEntity.setUser_id(bookingObject.getUser_id());
        bookingEntity.setDay_id(bookingObject.getDay_id());
        bookingEntity.setReference_number(bookingObject.getReference_number());
        return bookingEntity;
    }

    public List<Booking> toBookingObjectList(List<BookingEntity> bookingEntityList) {
        if (bookingEntityList == null) {
            return null;
        }
        List<Booking> bookingObjectList = new ArrayList<>();
        for (BookingEntity bookingEntity : bookingEntityList) {
            bookingObjectList.add(toBookingObject(bookingEntity));
        }
        return bookingObjectList;
    }

    public List<BookingEntity> toBookingEntityList(List<Booking> bookingObjectList) {
        if (bookingObjectList == null) {
            return null;
        }
        List<BookingEntity> bookingEntityList = new ArrayList<>();
        for (Booking bookingObject : bookingObjectList) {
            bookingEntityList.add(toBookingEntity(bookingObject));
        }
        return bookingEntityList;
    }

    public void updateBookingEntity(BookingEntity bookingEntity, Booking booking) {
        if (booking.getBooking_id() != null) {
            bookingEntity.setBooking_id(booking.getBooking_id());
        }
        bookingEntity.setDesk_id(booking.getDesk_id());
        bookingEntity.setUser_id(booking.getUser_id());
        bookingEntity.setDay_id(booking.getDay_id());
        bookingEntity.setReference_number(booking.getReference_number());
    }
}