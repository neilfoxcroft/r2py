package com.r2py.app.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "bookings")
@Getter
@Setter
public class BookingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer booking_id;

    @Column(name = "desk_id", columnDefinition = "integer")
    private Integer desk_id;

    @Column(name = "user_id", columnDefinition = "integer")
    private Integer user_id;

    @Column(name = "day_id", columnDefinition = "integer")
    private Integer day_id;

    @Column(name = "reference_number", columnDefinition = "nvarchar")
    private String reference_number;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name="desk_id", insertable = false, updatable = false)
//    private DeskEntity deskEntity;
//
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name="user_id", insertable = false, updatable = false)
//    private UserEntity userEntity;
//
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name="day_id", insertable = false, updatable = false)
//    private DayEntity dayEntity;
}
