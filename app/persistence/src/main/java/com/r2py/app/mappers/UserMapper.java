package com.r2py.app.mappers;

import com.r2py.app.beans.User;
import com.r2py.app.entities.UserEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    public User toUserObject(UserEntity userEntity){
        if (userEntity == null){
            return null;
        }
        User userObject = new User();
        if (userEntity.getUser_id() != null){
            userObject.setUser_id(userEntity.getUser_id());
        }
        userObject.setEmail_address(userEntity.getEmail_address());
        userObject.setFirst_name(userEntity.getFirst_name());
        userObject.setLast_name(userEntity.getLast_name());
        userObject.setPassword(userEntity.getPassword());
        userObject.setEmail_address(userEntity.getEmail_address());
        userObject.setTeam_id(userEntity.getTeam_id());
        return userObject;
    }

    public UserEntity toUserEntity(User userObject){
        if (userObject == null){
            return null;
        }
        UserEntity userEntity = new UserEntity();
        if (userObject.getUser_id() != null){
            userEntity.setUser_id(userObject.getUser_id());
        }
        userEntity.setFirst_name(userObject.getFirst_name());
        userEntity.setLast_name(userObject.getLast_name());
        userEntity.setEmail_address(userObject.getEmail_address());
        userEntity.setPassword(userObject.getPassword());
        userEntity.setTeam_id(userObject.getTeam_id());
        return userEntity;
    }

    public List<User> toUserObjectList(List<UserEntity> userEntityList){
        if (userEntityList == null){
            return null;
        }
        List<User> userObjectList = new ArrayList<>();
        for (UserEntity userEntity : userEntityList){
            userObjectList.add(toUserObject(userEntity));
        }
        return userObjectList;
    }

    public List<UserEntity> toUserEntityList(List<User> userObjectList){
        if (userObjectList == null){
            return null;
        }
        List<UserEntity> userEntityList = new ArrayList<>();
        for (User userObject : userObjectList){
            userEntityList.add(toUserEntity(userObject));
        }
        return userEntityList;
    }

    public void updateUserEntity(UserEntity userEntity, User user){
        if (user.getUser_id() != null){
            userEntity.setUser_id(user.getUser_id());
        }
        userEntity.setFirst_name(user.getFirst_name());
        userEntity.setLast_name(user.getLast_name());
        userEntity.setPassword(user.getPassword());
        userEntity.setEmail_address(user.getEmail_address());
        userEntity.setTeam_id(user.getTeam_id());
    }

}
