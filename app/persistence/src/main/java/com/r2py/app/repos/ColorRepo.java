package com.r2py.app.repos;

import com.r2py.app.beans.Color;
import com.r2py.app.entities.ColorEntity;
import com.r2py.app.interfaces.IRepo;
import com.r2py.app.mappers.ColorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class ColorRepo implements IRepo<Color, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IRepo.class.getName());

    @PersistenceContext
    EntityManager entityManager;
    ColorMapper colorMapper;

    @Autowired
    public ColorRepo(ColorMapper colorMapper, EntityManager entityManager) {
        this.colorMapper = colorMapper;
        this.entityManager = entityManager;
    }

    @Override
    public Color create(Color color) {
        entityManager.getTransaction().begin();
        ColorEntity colorEntity = colorMapper.toColorEntity(color);
        entityManager.persist(colorEntity);
        entityManager.getTransaction().commit();
        LOGGER.debug("Created color with ID: " + colorEntity.getColor_id());
        return colorMapper.toColorObject(colorEntity);
    }

    @Override
    public Color update(Color color) {
        entityManager.getTransaction().begin();
        ColorEntity colorEntity;
        if (color.getColor_id() == null){
            colorEntity = colorMapper.toColorEntity(color);
        } else {
            colorEntity = entityManager.find(ColorEntity.class, color.getColor_id());
            colorMapper.updateColorEntity(colorEntity, color);
        }
        entityManager.persist(colorEntity);
        entityManager.getTransaction().commit();
        return colorMapper.toColorObject(colorEntity);
    }

    @Override
    public Color delete(Color color) {
        entityManager.getTransaction().begin();
        entityManager.remove(color);
        entityManager.getTransaction().commit();
        return color;
    }

    @Override
    public Color findById(Integer id) {
        ColorEntity colorEntity = entityManager.find(ColorEntity.class, id);
        if (colorEntity == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return colorMapper.toColorObject(colorEntity);
    }

    @Override
    public List<Color> getAll() {
        return entityManager.createQuery("select color from ColorEntity color").getResultList();
    }
}
