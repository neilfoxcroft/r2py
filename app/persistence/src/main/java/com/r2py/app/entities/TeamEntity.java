package com.r2py.app.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "teams")
@Getter
@Setter
public class TeamEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer team_id;

    @Column(name = "office_id", columnDefinition = "integer")
    private Integer office_id;

    @Column(name = "team_name", columnDefinition = "nvarchar")
    private String team_name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private List<UserEntity> userEntities;
}
