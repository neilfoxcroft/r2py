package com.r2py.app.repos;

import com.r2py.app.beans.Booking;
import com.r2py.app.entities.BookingEntity;
import com.r2py.app.interfaces.IRepo;
import com.r2py.app.mappers.BookingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class BookingRepo implements IRepo<Booking, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IRepo.class.getName());

    @PersistenceContext
    EntityManager entityManager;
    BookingMapper bookingMapper;

    @Autowired
    public BookingRepo(BookingMapper bookingMapper, EntityManager entityManager) {
        this.bookingMapper = bookingMapper;
        this.entityManager = entityManager;
    }

    @Transactional
    public Booking create(Booking booking) {
        BookingEntity bookingEntity = bookingMapper.toBookingEntity(booking);
        entityManager.persist(bookingEntity);
        LOGGER.debug("Created booking with ID: " + bookingEntity.getBooking_id());
        return bookingMapper.toBookingObject(bookingEntity);
    }

    @Transactional
    public Booking delete(Booking booking) {
        BookingEntity bookingEntity = bookingMapper.toBookingEntity(booking);
        entityManager.remove(entityManager.contains(bookingEntity) ? bookingEntity : entityManager.merge(bookingEntity));
        return bookingMapper.toBookingObject(bookingEntity);
    }

    @Transactional
    public Booking update(Booking booking) {
        BookingEntity bookingEntity;
        if (booking.getBooking_id() == null){
            bookingEntity = bookingMapper.toBookingEntity(booking);
        } else {
            bookingEntity = entityManager.find(BookingEntity.class, booking.getBooking_id());
            bookingMapper.updateBookingEntity(bookingEntity, booking);
        }
        entityManager.persist(bookingEntity);
        return bookingMapper.toBookingObject(bookingEntity);
    }

    @Override
    public Booking findById(Integer id) {
        BookingEntity bookingEntity = entityManager.find(BookingEntity.class, id);
        if (bookingEntity == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return bookingMapper.toBookingObject(bookingEntity);
    }

    @Override
    public List<Booking> getAll() {
        return entityManager.createQuery("select booking from BookingEntity booking").getResultList();
    }
}
