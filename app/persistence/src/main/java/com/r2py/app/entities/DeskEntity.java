package com.r2py.app.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "desks")
@Getter
@Setter
public class DeskEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer desk_id;

    @Column(name = "desk_number", columnDefinition = "nvarchar")
    private String desk_number;

    @Column(name = "x_cord", columnDefinition = "integer")
    private Integer x_cord;

    @Column(name = "y_cord", columnDefinition = "integer")
    private Integer y_cord;

    @Column(name = "group_number", columnDefinition = "integer")
    private Integer group_number;

    @Column(name = "booked", columnDefinition = "bit")
    private Boolean booked;

    @Column(name = "office_id", columnDefinition = "integer")
    private Integer office_id;

    @Column(name = "color_id", columnDefinition = "integer")
    private Integer color_id;
}
