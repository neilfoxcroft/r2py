package com.r2py.app.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
@Getter
@Setter
public class UserEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer user_id;


    @Column(name = "first_name", columnDefinition = "nvarchar")
    private String first_name;

    @Column(name = "last_name", columnDefinition = "nvarchar")
    private String last_name;

    @Column(name = "email_address", columnDefinition = "nvarchar")
    private String email_address;

    @Column(name = "password", columnDefinition = "nvarchar")
    private String password;

    @Column(name = "team_id", columnDefinition = "integer")
    private Integer team_id;

//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    @JoinColumn(name = "team_id")
//    private TeamEntity teamEntity;
}
