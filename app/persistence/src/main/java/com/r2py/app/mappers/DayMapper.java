package com.r2py.app.mappers;

import com.r2py.app.beans.Day;
import com.r2py.app.entities.DayEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DayMapper {

    public Day toDayObject(DayEntity dayEntity) {
        if (dayEntity == null) {
            return null;
        }
        Day dayObject = new Day();
        if (dayEntity.getDay_id() != null) {
            dayObject.setDay_id(dayEntity.getDay_id());
        }
        dayObject.setDate(dayEntity.getDate());
        return dayObject;
    }

    public DayEntity toDayEntity(Day dayObject) {
        if (dayObject == null) {
            return null;
        }
        DayEntity dayEntity = new DayEntity();
        if (dayObject.getDay_id() != null) {
            dayEntity.setDay_id(dayObject.getDay_id());
        }
        dayEntity.setDate(dayObject.getDate());
        return dayEntity;
    }

    public List<Day> toDayObjectList(List<DayEntity> dayEntityList) {
        if (dayEntityList == null) {
            return null;
        }
        List<Day> dayObjectList = new ArrayList<>();
        for (DayEntity dayEntity : dayEntityList) {
            dayObjectList.add(toDayObject(dayEntity));
        }
        return dayObjectList;
    }

    public List<DayEntity> toDayEntityList(List<Day> dayObjectList) {
        if (dayObjectList == null) {
            return null;
        }
        List<DayEntity> dayEntityList = new ArrayList<>();
        for (Day dayObject : dayObjectList) {
            dayEntityList.add(toDayEntity(dayObject));
        }
        return dayEntityList;
    }

    public void updateDayEntity(DayEntity dayEntity, Day day) {
        if (day.getDay_id() != null) {
            dayEntity.setDay_id(day.getDay_id());
        }
        dayEntity.setDate(day.getDate());
    }
}