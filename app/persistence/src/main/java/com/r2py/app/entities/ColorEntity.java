package com.r2py.app.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "colors")
@Getter
@Setter
public class ColorEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer color_id;

    @Column(name = "color", columnDefinition = "nvarchar")
    private String color;
}
