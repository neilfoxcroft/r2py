package com.r2py.app.repos;

import com.r2py.app.beans.Day;
import com.r2py.app.entities.DayEntity;
import com.r2py.app.interfaces.IRepo;
import com.r2py.app.mappers.DayMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class DayRepo implements IRepo<Day, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IRepo.class.getName());

    @PersistenceContext
    EntityManager entityManager;
    DayMapper dayMapper;

    @Autowired
    public DayRepo(DayMapper dayMapper, EntityManager entityManager) {
        this.dayMapper = dayMapper;
        this.entityManager = entityManager;
    }

    @Override
    public Day create(Day day) {
        return null;
    }

    @Override
    public Day update(Day day) {
        return null;
    }

    @Override
    public Day delete(Day day) {
        return null;
    }

    @Override
    public Day findById(Integer id) {
        DayEntity dayEntity = entityManager.find(DayEntity.class, id);
        if (dayEntity == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return dayMapper.toDayObject(dayEntity);
    }

    @Override
    public List<Day> getAll() {
        return entityManager.createQuery("select day from DayEntity day").getResultList();
    }
}
