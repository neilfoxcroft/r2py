package com.r2py.app.repos;

import com.r2py.app.beans.Team;
import com.r2py.app.entities.TeamEntity;
import com.r2py.app.interfaces.IRepo;
import com.r2py.app.mappers.TeamMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class TeamRepo implements IRepo<Team, Integer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IRepo.class.getName());

    @PersistenceContext
    EntityManager entityManager;
    TeamMapper teamMapper;

    @Autowired
    public TeamRepo(TeamMapper teamMapper, EntityManager entityManager) {
        this.teamMapper = teamMapper;
        this.entityManager = entityManager;
    }

    @Transactional
    public Team create(Team team) {
        TeamEntity teamEntity = teamMapper.toTeamEntity(team);
        entityManager.persist(teamEntity);
        LOGGER.debug("Created team with ID: " + teamEntity.getTeam_id());
        return teamMapper.toTeamObject(teamEntity);
    }

    @Transactional
    public Team delete(Team team) {
        TeamEntity teamEntity = teamMapper.toTeamEntity(team);
        entityManager.remove(entityManager.contains(teamEntity) ? teamEntity : entityManager.merge(teamEntity));
        return teamMapper.toTeamObject(teamEntity);
    }

    @Transactional
    public Team update(Team team) {
        TeamEntity teamEntity;
        if (team.getTeam_id() == null) {
            teamEntity = teamMapper.toTeamEntity(team);
        } else {
            teamEntity = entityManager.find(TeamEntity.class, team.getTeam_id());
            teamMapper.updateTeamEntity(teamEntity, team);
        }
        entityManager.persist(teamEntity);
        return teamMapper.toTeamObject(teamEntity);
    }

    @Override
    public Team findById(Integer id) {
        TeamEntity teamEntity = entityManager.find(TeamEntity.class, id);
        if (teamEntity == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return teamMapper.toTeamObject(teamEntity);
    }

    @Override
    public List<Team> getAll() {
        List<TeamEntity> teamEntities = entityManager.createQuery("SELECT team FROM TeamEntity team").getResultList();
        if (teamEntities == null){
            entityManager.getTransaction().rollback();
            throw new EntityNotFoundException();
        }
        return teamMapper.toTeamObjectList(teamEntities);
    }
}
