create table colors
(
    color_id int identity
        constraint colors_pk
            primary key,
    color    nvarchar(25) not null
)
go

create unique index colors_color_id_uindex
    on colors (color_id)
go

INSERT INTO r2py.dbo.colors (color_id, color) VALUES (3, N'Green');
INSERT INTO r2py.dbo.colors (color_id, color) VALUES (4, N'Blue');
