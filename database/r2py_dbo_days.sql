create table days
(
    day_id int identity
        constraint days_pk
            primary key,
    date   date not null
)
go

INSERT INTO r2py.dbo.days (day_id, date) VALUES (1, N'2022-01-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (2, N'2022-01-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (3, N'2022-01-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (4, N'2022-01-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (5, N'2022-01-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (6, N'2022-01-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (7, N'2022-01-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (8, N'2022-01-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (9, N'2022-01-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (10, N'2022-01-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (11, N'2022-01-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (12, N'2022-01-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (13, N'2022-01-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (14, N'2022-01-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (15, N'2022-01-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (16, N'2022-01-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (17, N'2022-01-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (18, N'2022-01-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (19, N'2022-01-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (20, N'2022-01-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (21, N'2022-01-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (22, N'2022-01-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (23, N'2022-01-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (24, N'2022-01-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (25, N'2022-01-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (26, N'2022-01-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (27, N'2022-01-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (28, N'2022-01-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (29, N'2022-01-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (30, N'2022-01-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (31, N'2022-01-31');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (32, N'2022-02-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (33, N'2022-02-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (34, N'2022-02-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (35, N'2022-02-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (36, N'2022-02-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (37, N'2022-02-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (38, N'2022-02-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (39, N'2022-02-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (40, N'2022-02-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (41, N'2022-02-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (42, N'2022-02-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (43, N'2022-02-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (44, N'2022-02-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (45, N'2022-02-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (46, N'2022-02-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (47, N'2022-02-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (48, N'2022-02-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (49, N'2022-02-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (50, N'2022-02-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (51, N'2022-02-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (52, N'2022-02-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (53, N'2022-02-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (54, N'2022-02-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (55, N'2022-02-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (56, N'2022-02-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (57, N'2022-02-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (58, N'2022-02-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (59, N'2022-02-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (60, N'2022-03-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (61, N'2022-03-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (62, N'2022-03-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (63, N'2022-03-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (64, N'2022-03-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (65, N'2022-03-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (66, N'2022-03-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (67, N'2022-03-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (68, N'2022-03-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (69, N'2022-03-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (70, N'2022-03-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (71, N'2022-03-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (72, N'2022-03-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (73, N'2022-03-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (74, N'2022-03-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (75, N'2022-03-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (76, N'2022-03-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (77, N'2022-03-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (78, N'2022-03-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (79, N'2022-03-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (80, N'2022-03-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (81, N'2022-03-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (82, N'2022-03-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (83, N'2022-03-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (84, N'2022-03-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (85, N'2022-03-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (86, N'2022-03-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (87, N'2022-03-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (88, N'2022-03-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (89, N'2022-03-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (90, N'2022-03-31');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (91, N'2022-04-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (92, N'2022-04-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (93, N'2022-04-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (94, N'2022-04-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (95, N'2022-04-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (96, N'2022-04-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (97, N'2022-04-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (98, N'2022-04-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (99, N'2022-04-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (100, N'2022-04-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (101, N'2022-04-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (102, N'2022-04-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (103, N'2022-04-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (104, N'2022-04-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (105, N'2022-04-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (106, N'2022-04-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (107, N'2022-04-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (108, N'2022-04-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (109, N'2022-04-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (110, N'2022-04-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (111, N'2022-04-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (112, N'2022-04-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (113, N'2022-04-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (114, N'2022-04-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (115, N'2022-04-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (116, N'2022-04-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (117, N'2022-04-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (118, N'2022-04-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (119, N'2022-04-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (120, N'2022-04-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (121, N'2022-05-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (122, N'2022-05-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (123, N'2022-05-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (124, N'2022-05-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (125, N'2022-05-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (126, N'2022-05-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (127, N'2022-05-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (128, N'2022-05-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (129, N'2022-05-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (130, N'2022-05-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (131, N'2022-05-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (132, N'2022-05-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (133, N'2022-05-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (134, N'2022-05-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (135, N'2022-05-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (136, N'2022-05-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (137, N'2022-05-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (138, N'2022-05-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (139, N'2022-05-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (140, N'2022-05-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (141, N'2022-05-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (142, N'2022-05-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (143, N'2022-05-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (144, N'2022-05-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (145, N'2022-05-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (146, N'2022-05-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (147, N'2022-05-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (148, N'2022-05-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (149, N'2022-05-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (150, N'2022-05-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (151, N'2022-05-31');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (152, N'2022-06-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (153, N'2022-06-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (154, N'2022-06-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (155, N'2022-06-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (156, N'2022-06-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (157, N'2022-06-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (158, N'2022-06-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (159, N'2022-06-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (160, N'2022-06-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (161, N'2022-06-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (162, N'2022-06-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (163, N'2022-06-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (164, N'2022-06-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (165, N'2022-06-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (166, N'2022-06-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (167, N'2022-06-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (168, N'2022-06-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (169, N'2022-06-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (170, N'2022-06-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (171, N'2022-06-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (172, N'2022-06-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (173, N'2022-06-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (174, N'2022-06-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (175, N'2022-06-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (176, N'2022-06-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (177, N'2022-06-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (178, N'2022-06-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (179, N'2022-06-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (180, N'2022-06-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (181, N'2022-06-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (182, N'2022-07-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (183, N'2022-07-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (184, N'2022-07-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (185, N'2022-07-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (186, N'2022-07-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (187, N'2022-07-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (188, N'2022-07-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (189, N'2022-07-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (190, N'2022-07-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (191, N'2022-07-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (192, N'2022-07-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (193, N'2022-07-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (194, N'2022-07-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (195, N'2022-07-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (196, N'2022-07-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (197, N'2022-07-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (198, N'2022-07-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (199, N'2022-07-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (200, N'2022-07-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (201, N'2022-07-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (202, N'2022-07-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (203, N'2022-07-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (204, N'2022-07-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (205, N'2022-07-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (206, N'2022-07-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (207, N'2022-07-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (208, N'2022-07-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (209, N'2022-07-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (210, N'2022-07-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (211, N'2022-07-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (212, N'2022-07-31');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (213, N'2022-08-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (214, N'2022-08-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (215, N'2022-08-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (216, N'2022-08-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (217, N'2022-08-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (218, N'2022-08-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (219, N'2022-08-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (220, N'2022-08-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (221, N'2022-08-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (222, N'2022-08-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (223, N'2022-08-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (224, N'2022-08-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (225, N'2022-08-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (226, N'2022-08-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (227, N'2022-08-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (228, N'2022-08-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (229, N'2022-08-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (230, N'2022-08-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (231, N'2022-08-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (232, N'2022-08-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (233, N'2022-08-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (234, N'2022-08-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (235, N'2022-08-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (236, N'2022-08-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (237, N'2022-08-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (238, N'2022-08-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (239, N'2022-08-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (240, N'2022-08-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (241, N'2022-08-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (242, N'2022-08-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (243, N'2022-08-31');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (244, N'2022-09-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (245, N'2022-09-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (246, N'2022-09-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (247, N'2022-09-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (248, N'2022-09-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (249, N'2022-09-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (250, N'2022-09-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (251, N'2022-09-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (252, N'2022-09-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (253, N'2022-09-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (254, N'2022-09-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (255, N'2022-09-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (256, N'2022-09-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (257, N'2022-09-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (258, N'2022-09-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (259, N'2022-09-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (260, N'2022-09-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (261, N'2022-09-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (262, N'2022-09-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (263, N'2022-09-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (264, N'2022-09-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (265, N'2022-09-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (266, N'2022-09-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (267, N'2022-09-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (268, N'2022-09-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (269, N'2022-09-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (270, N'2022-09-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (271, N'2022-09-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (272, N'2022-09-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (273, N'2022-09-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (274, N'2022-10-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (275, N'2022-10-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (276, N'2022-10-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (277, N'2022-10-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (278, N'2022-10-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (279, N'2022-10-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (280, N'2022-10-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (281, N'2022-10-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (282, N'2022-10-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (283, N'2022-10-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (284, N'2022-10-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (285, N'2022-10-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (286, N'2022-10-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (287, N'2022-10-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (288, N'2022-10-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (289, N'2022-10-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (290, N'2022-10-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (291, N'2022-10-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (292, N'2022-10-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (293, N'2022-10-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (294, N'2022-10-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (295, N'2022-10-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (296, N'2022-10-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (297, N'2022-10-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (298, N'2022-10-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (299, N'2022-10-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (300, N'2022-10-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (301, N'2022-10-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (302, N'2022-10-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (303, N'2022-10-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (304, N'2022-10-31');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (305, N'2022-11-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (306, N'2022-11-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (307, N'2022-11-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (308, N'2022-11-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (309, N'2022-11-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (310, N'2022-11-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (311, N'2022-11-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (312, N'2022-11-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (313, N'2022-11-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (314, N'2022-11-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (315, N'2022-11-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (316, N'2022-11-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (317, N'2022-11-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (318, N'2022-11-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (319, N'2022-11-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (320, N'2022-11-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (321, N'2022-11-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (322, N'2022-11-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (323, N'2022-11-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (324, N'2022-11-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (325, N'2022-11-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (326, N'2022-11-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (327, N'2022-11-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (328, N'2022-11-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (329, N'2022-11-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (330, N'2022-11-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (331, N'2022-11-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (332, N'2022-11-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (333, N'2022-11-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (334, N'2022-11-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (335, N'2022-12-01');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (336, N'2022-12-02');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (337, N'2022-12-03');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (338, N'2022-12-04');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (339, N'2022-12-05');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (340, N'2022-12-06');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (341, N'2022-12-07');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (342, N'2022-12-08');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (343, N'2022-12-09');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (344, N'2022-12-10');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (345, N'2022-12-11');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (346, N'2022-12-12');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (347, N'2022-12-13');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (348, N'2022-12-14');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (349, N'2022-12-15');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (350, N'2022-12-16');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (351, N'2022-12-17');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (352, N'2022-12-18');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (353, N'2022-12-19');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (354, N'2022-12-20');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (355, N'2022-12-21');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (356, N'2022-12-22');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (357, N'2022-12-23');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (358, N'2022-12-24');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (359, N'2022-12-25');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (360, N'2022-12-26');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (361, N'2022-12-27');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (362, N'2022-12-28');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (363, N'2022-12-29');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (364, N'2022-12-30');
INSERT INTO r2py.dbo.days (day_id, date) VALUES (365, N'2022-12-31');
