create table offices
(
    office_id      int identity
        constraint offices_pk
            primary key,
    office_name    nvarchar(255) not null,
    office_address nvarchar(255) not null
)
go

create unique index offices_office_id_uindex
    on offices (office_id)
go

INSERT INTO r2py.dbo.offices (office_id, office_name, office_address) VALUES (1, N'Entelect HQ', N'Unit 13, 3 Melrose Blvd, Melrose, Johannesburg, 2196');
INSERT INTO r2py.dbo.offices (office_id, office_name, office_address) VALUES (2, N'Entelect Pretoria', N'Southdowns Ridge Office Park Corner Nellmapius &, John Vorster Dr, Irene, Centurion, 0062');
INSERT INTO r2py.dbo.offices (office_id, office_name, office_address) VALUES (3, N'Entelect Cape Town', N'Building Floor 3, 1 Century City Dr, Century City, Cape Town, 7441');
