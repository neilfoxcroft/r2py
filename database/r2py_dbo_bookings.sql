create table bookings
(
    booking_id       int identity,
    desk_id          int           not null
        constraint bookings_desks___fk
            references desks,
    user_id          int           not null
        constraint bookings_users___fk
            references users,
    day_id           int           not null
        constraint bookings_days___fk
            references days,
    reference_number nvarchar(255) not null
)
go

create unique index bookings_booking_id_uindex
    on bookings (booking_id)
go

create unique index bookings_reference_number_uindex
    on bookings (reference_number)
go

