create table teams
(
    team_id   int identity
        constraint teams_pk
            primary key,
    office_id int           not null
        constraint teams_office___fk
            references offices,
    team_name nvarchar(255) not null
)
go

create unique index teams_team_id_uindex
    on teams (team_id)
go

INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (1, 1, N'Jango Fett');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (2, 1, N'Jabba the Hut');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (3, 1, N'Jar Jar Binks');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (4, 2, N'Padawan');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (5, 2, N'Padmé');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (6, 2, N'Palpatin');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (7, 3, N'C3PO');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (8, 3, N'Clone');
INSERT INTO r2py.dbo.teams (team_id, office_id, team_name) VALUES (9, 3, N'Count Dooku');
