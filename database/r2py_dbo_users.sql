create table users
(
    user_id       int identity
        constraint users_pk
            primary key,
    first_name    nvarchar(255) not null,
    last_name     nvarchar(255) not null,
    email_address nvarchar(255),
    password      nvarchar(255) not null,
    team_id       int
        constraint users_team___fk
            references teams
)
go

create unique index users_user_id_uindex
    on users (user_id)
go

INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1, N'Arabella', N'Cuttell', N'acuttell0@yolasite.com', N'pLdGcwPcQ84p', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (2, N'Zeke', N'Roseblade', N'zroseblade1@linkedin.com', N'KCyIKNKEIi', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (3, N'Nolie', N'Castelain', N'ncastelain2@webeden.co.uk', N'Mnkkq00bIKNM', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (4, N'Koressa', N'Troy', N'ktroy3@about.com', N'kWO5Fvsgwjr', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (5, N'Mariya', N'Nairns', N'mnairns4@pen.io', N'ClbeUiNBePw', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (6, N'Idaline', N'Tiner', N'itiner5@omniture.com', N'4kldHGg9tT', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (7, N'Cristina', N'Ricciardi', N'cricciardi6@mac.com', N'XLablsTHd5n', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (8, N'Sylas', N'Blaskett', N'sblaskett7@walmart.com', N'2xkDFthu6R', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (9, N'Thomas', N'Ahlf', N'tahlf8@tumblr.com', N'3k10g5GEXp', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (10, N'Celestyna', N'Fernez', N'cfernez9@walmart.com', N'mzT3G0I4Ut', 1);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (11, N'Lisle', N'Slingsby', N'lslingsbya@cam.ac.uk', N'P45EyEagmA', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (12, N'Con', N'Marler', N'cmarlerb@upenn.edu', N'wlxWYsd', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (13, N'Jerrie', N'Graham', N'jgrahamc@alexa.com', N'1IEsYP', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (14, N'Tanitansy', N'Mertsching', N'tmertschingd@e-recht24.de', N'SEGLxFbe', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (15, N'Darrick', N'Millam', N'dmillame@nasa.gov', N'1OTuAn', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (16, N'Kaia', N'Snell', N'ksnellf@telegraph.co.uk', N'puv8cw62zfD', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (17, N'Kamillah', N'MacAloren', N'kmacaloreng@utexas.edu', N'xSG07D', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (18, N'Bobbie', N'Stittle', N'bstittleh@blogtalkradio.com', N'R4zDU7M5A', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (19, N'Valencia', N'Humerstone', N'vhumerstonei@goo.ne.jp', N'HyfVVvX2A', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (20, N'Mable', N'Siaskowski', N'msiaskowskij@blogspot.com', N'zQso4Yk', 2);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (21, N'Maison', N'Pieterick', N'mpieterickk@jiathis.com', N'7Pw7eLH', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (22, N'Leonardo', N'Memory', N'lmemoryl@weather.com', N'mRHIQdqm', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (23, N'Lorilee', N'Dimmock', N'ldimmockm@alibaba.com', N'dfktUwnN16zw', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (24, N'Trace', N'Longmire', N'tlongmiren@state.gov', N'c5yz4ME75ep', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (25, N'Evangelia', N'Gouge', N'egougeo@blinklist.com', N'sJo8VD', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (26, N'Peg', N'Lodin', N'plodinp@scribd.com', N'oUM1YXQn', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (27, N'Findley', N'Sperski', N'fsperskiq@mtv.com', N'ZbgokswwoqXL', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (28, N'Mickey', N'Yosevitz', N'myosevitzr@narod.ru', N'Qs8fHio', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (29, N'Raimondo', N'Skellorne', N'rskellornes@home.pl', N'Bm6sAHx3U', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (30, N'Peri', N'Roskams', N'proskamst@ameblo.jp', N'MXd3VF8', 3);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (31, N'Meris', N'Gransden', N'mgransdenu@nhs.uk', N'iQNOWhMmx', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (32, N'Chrotoem', N'Miskimmon', N'cmiskimmonv@seesaa.net', N'XqJMnDuszRu', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (33, N'Adrienne', N'Josifovic', N'ajosifovicw@census.gov', N'9Uf2rA', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (34, N'Shelagh', N'Lyons', N'slyonsx@umn.edu', N'UCX6V00K4WK2', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (35, N'Henryetta', N'Piggot', N'hpiggoty@github.com', N'vgqfzVk7', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (36, N'Laverna', N'Isaksen', N'lisaksenz@hatena.ne.jp', N'XS75AiawowC', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (37, N'Kippie', N'Piatkowski', N'kpiatkowski10@g.co', N'bpQ7YhL', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (38, N'Bank', N'Downie', N'bdownie11@123-reg.co.uk', N'XLmZVnfwjp', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (39, N'Worthy', N'Collinson', N'wcollinson12@seesaa.net', N'rueYbG', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (40, N'Genevra', N'Daugherty', N'gdaugherty13@statcounter.com', N'DhNOF60', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (41, N'Raymund', N'Everist', N'reverist14@cocolog-nifty.com', N'DEPRmZls', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (42, N'Claude', N'Bartram', N'cbartram15@godaddy.com', N'Ql7wdR', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (43, N'Magdalena', N'Creddon', N'mcreddon16@w3.org', N'zK7RG5mt4lW', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (44, N'Nefen', N'Escott', N'nescott17@slashdot.org', N'cU0rX0k2', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (45, N'Bud', N'Hrinchenko', N'bhrinchenko18@ocn.ne.jp', N'HE3T5h9L28np', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (46, N'Ozzy', N'McCandie', N'omccandie19@dell.com', N'DEQFo3', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (47, N'Cristobal', N'Pethick', N'cpethick1a@lycos.com', N'rEsIqyEnr9OG', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (48, N'Ettore', N'Clewes', N'eclewes1b@google.com.br', N'oXkW5U0', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (49, N'Quintus', N'Bungey', N'qbungey1c@columbia.edu', N'unHqjieZ3lq', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (50, N'Xylina', N'Heppenspall', N'xheppenspall1d@nps.gov', N'WakKhS7', 4);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (51, N'Norby', N'Langston', N'nlangston1e@sakura.ne.jp', N'34c5AWQuEUm', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (52, N'Maggy', N'McTrustey', N'mmctrustey1f@phoca.cz', N'92mWm0b', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (53, N'Ferdinanda', N'Strelitzer', N'fstrelitzer1g@ask.com', N'RoP2WzkvROO', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (54, N'Ebeneser', N'Edowes', N'eedowes1h@themeforest.net', N'UmvalGAGNnB', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (55, N'Carolann', N'Eburne', N'ceburne1i@gravatar.com', N'O558xyY7ns6q', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (56, N'Marchelle', N'Paddon', N'mpaddon1j@ftc.gov', N'RLpUjKpkE', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (57, N'Theodore', N'Lehrian', N'tlehrian1k@yelp.com', N'Xq3LkAYFNQ5d', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (58, N'Tobey', N'Balloch', N'tballoch1l@wired.com', N'eRNikgBzx0', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (59, N'Grace', N'Cranmor', N'gcranmor1m@disqus.com', N'x1vQ6j', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (60, N'Eveleen', N'Melland', N'emelland1n@jimdo.com', N'WV0057bP3Q', 5);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (61, N'Ken', N'Yanne', N'kyanne1o@nba.com', N'B8lWp72kQ6', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (62, N'Rhoda', N'Willbraham', N'rwillbraham1p@java.com', N'tnYPF8lb3', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (63, N'Brant', N'Witling', N'bwitling1q@cbc.ca', N'Af7RlQLNex56', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (64, N'Kellsie', N'Joynt', N'kjoynt1r@globo.com', N'm4AmAPCL3', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (65, N'Andres', N'Swalwell', N'aswalwell1s@icio.us', N'b0HAI7ox5D5', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (66, N'Ricki', N'Furnell', N'rfurnell1t@sogou.com', N'PVsjQPvw', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (67, N'Clarence', N'Reynolds', N'creynolds1u@dot.gov', N'qCaWWo5eN9P', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (68, N'Lambert', N'Saice', N'lsaice1v@kickstarter.com', N'3oSgKwFS', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (69, N'Bengt', N'Michelle', N'bmichelle1w@smh.com.au', N'iPwRCICtM', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (70, N'Saul', N'Mounsey', N'smounsey1x@sourceforge.net', N'XmkAap1VjGC', 6);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (71, N'Bethanne', N'Tendahl', N'btendahl1y@wix.com', N'V1D1h7tKTqVA', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (72, N'Christel', N'Mayer', N'cmayer1z@ucoz.com', N'5nPwPxW2RR', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (73, N'Wang', N'Cherryman', N'wcherryman20@behance.net', N'537PHKlt3', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (74, N'Alessandra', N'McKay', N'amckay21@patch.com', N'KxoDMIayChQx', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (75, N'Fifi', N'MacTeggart', N'fmacteggart22@ox.ac.uk', N'sYHMR6adoaQK', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (76, N'Lauri', N'Reidshaw', N'lreidshaw23@comsenz.com', N'6FjhZuHb', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (77, N'Norrie', N'McCaughey', N'nmccaughey24@liveinternet.ru', N'qFHqLwGPcl', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (78, N'Alvan', N'Kollas', N'akollas25@google.ca', N'CPG8fVakAlDZ', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (79, N'Lotta', N'Wilcher', N'lwilcher26@friendfeed.com', N'komE2PlP1tq', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (80, N'Bastien', N'Greensite', N'bgreensite27@booking.com', N'S5Kro4G1ol', 7);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (81, N'Cy', N'Nissle', N'cnissle28@vistaprint.com', N'jwEOJWD5P', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (82, N'Elsinore', N'Heminsley', N'eheminsley29@symantec.com', N'u9P3uL5ZH3T', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (83, N'Pryce', N'Ziebart', N'pziebart2a@1und1.de', N'CgZ8FQB', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (84, N'Libby', N'Toler', N'ltoler2b@merriam-webster.com', N'tGDSR9TRxQ', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (85, N'Sheena', N'Wadesworth', N'swadesworth2c@tripod.com', N'lz4c8fpcY2H', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (86, N'Hi', N'Clementson', N'hclementson2d@uiuc.edu', N'UqCZrb739oIV', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (87, N'Rhys', N'Corre', N'rcorre2e@illinois.edu', N'nq8c10qG1F4d', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (88, N'Yehudit', N'Baildon', N'ybaildon2f@flickr.com', N'eKPSejLO4y', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (89, N'Eyde', N'Titchen', N'etitchen2g@sphinn.com', N'VTltj663', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (90, N'Deidre', N'McCurry', N'dmccurry2h@ehow.com', N'y6ARL6VAa', 8);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1001, N'Phyllis', N'Enders', N'penders0@github.com', N'sNRBiqmUVB', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1002, N'Garvy', N'McKenny', N'gmckenny1@independent.co.uk', N'fYfSKMGt', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1003, N'Drew', N'Owttrim', N'dowttrim2@altervista.org', N'a4cjLW', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1004, N'Rosaleen', N'Gosdin', N'rgosdin3@paginegialle.it', N'un3IIfwWcd', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1005, N'Bjorn', N'Salsbury', N'bsalsbury4@intel.com', N'1MmglS4Z', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1006, N'Raymund', N'Doumerque', N'rdoumerque5@sitemeter.com', N'vxiuYpjm', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1007, N'Brandy', N'Cheine', N'bcheine6@w3.org', N'YbEbqjJEiL', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1008, N'Kristen', N'Holdin', N'kholdin7@simplemachines.org', N'hSwhTxBSpVJ', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1009, N'Michaella', N'Kedge', N'mkedge8@odnoklassniki.ru', N'7J9sBT4VPG', 9);
INSERT INTO r2py.dbo.users (user_id, first_name, last_name, email_address, password, team_id) VALUES (1010, N'Alasteir', N'Cockram', N'acockram9@dmoz.org', N'6CxUTvhbEJ', 9);
