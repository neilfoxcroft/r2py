create table desks
(
    desk_id      int identity
        constraint desks_pk
            primary key,
    desk_number  varchar(25) not null,
    x_cord       int,
    y_cord       int,
    group_number int,
    booked       bit,
    office_id    int
        constraint desks_office___fk
            references offices,
    color_id     int
        constraint desks_colors___fk
            references colors
)
go

create unique index desks_desk_id_uindex
    on desks (desk_id)
go

create unique index desks_desk_number_uindex
    on desks (desk_number)
go

INSERT INTO r2py.dbo.desks (desk_id, desk_number, x_cord, y_cord, group_number, booked, office_id, color_id) VALUES (1, N'1-1-1-G', 1, 1, 1, 0, 1, 3);
INSERT INTO r2py.dbo.desks (desk_id, desk_number, x_cord, y_cord, group_number, booked, office_id, color_id) VALUES (2, N'1-2-1-B', 1, 2, 1, 0, 1, 4);
INSERT INTO r2py.dbo.desks (desk_id, desk_number, x_cord, y_cord, group_number, booked, office_id, color_id) VALUES (3, N'1-3-1-G', 1, 3, 1, 0, 1, 3);
